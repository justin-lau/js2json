#!/usr/bin/env node
'use strict';

const fs = require('fs');
const path = require('path');

function resolveNestedObjects(jsObject) {
  if (typeof jsObject !== 'object') {
    return jsObject;
  }

  for (const key in jsObject) {
    if (key.includes('.')) {
      const keys = key.split('.');
      const objRefs = [jsObject[keys[0]] ? jsObject[keys[0]] : {}];

      for (let i = 1; i < keys.length - 1; ++i) {
        if (!objRefs[i - 1][keys[i]]) {
          objRefs[i - 1][keys[i]] = {};
        }

        objRefs.push(objRefs[i - 1][keys[i]]);
      }

      objRefs[keys.length - 2][keys[keys.length - 1]] = resolveNestedObjects(jsObject[key]);

      jsObject[keys[0]] = objRefs[0];
      delete jsObject[key];
    }
  }

  return jsObject;
}

function readObjectFromFile(filename) {
  // test accessibility
  fs.accessSync(inputFilename, fs.F_OK | fs.R_OK);

  // create .tmp dir if not already exist
  try {
    fs.accessSync(`${__dirname}/.tmp`, fs.F_OK);
  } catch (error) {
    fs.mkdirSync(`${__dirname}/.tmp`);
  }

  const basename = path.basename(filename);
  const fileContent = fs.readFileSync(filename, { encoding: 'UTF-8' });

  // patch the file to replace `export default` with `module.exports`
  const tmpFilename = `${__dirname}/.tmp/${basename}`;
  const fd = fs.openSync(tmpFilename, 'w');
  fs.writeSync(fd, fileContent.replace('export default', 'module.exports ='));

  const jsObject = require(tmpFilename);

  // clean up
  fs.unlinkSync(tmpFilename);

  return jsObject;
}

const inputFilename = process.argv[2];
const outputFilename = process.argv[3];

const originalJSObject = readObjectFromFile(inputFilename);
const jsonString = JSON.stringify(resolveNestedObjects(originalJSObject), null, 4);

if (outputFilename) {
  const fd = fs.openSync(outputFilename, 'w');
  fs.writeSync(fd, jsonString);
} else {
  console.log(jsonString);
}

console.log('done!');
