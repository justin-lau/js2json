# js2json

## Installation

```bash
$ npm install -g https://gitlab.com/justin-lau/js2json.git
```

## Usage

```bash
$ js2json {inputPath} {outputPath}
```

If the `outputPath` is skipped the converted JSON will be printed to the console.
